class Video {
  final bool success;
  final String message;
  final List<dynamic> data;

  Video({
    this.success,
    this.message,
    this.data
  });

  factory Video.fromJson(Map<String, dynamic> json){
    return Video(
      success: json['success'],
      message: json['message'],
      data: json['data']
    );
  }

  @override
  String toString() {
    return 'Video{success: $success, message: $message, data: $data}';
  }
}

class VideoData {
  int id;
  String title;
  String price;
  String duration;
  String resolution;
  String uploader;
  String description;
  String thumbnail;
  String video;

  VideoData({this.id, this.title, this.price, this.duration, this.resolution, this.uploader, this.description, this.thumbnail, this.video});

  factory VideoData.fromJson(Map<String, dynamic> json) {
    return VideoData(
        id: json['id'],
        title: json['title'],
        duration: json['duration'],
        price: json['price'],
        resolution: json['resolution'],
        uploader: json['uploader'],
        description: json['description'],
        thumbnail: json['thumbnail'],
        video: json['video']
        );
  }

  @override
  String toString() {
    return 'VideoData{id: $id, title: $title, price: $price, duration: $duration, resolution: $resolution, uploader: $uploader, description: $description, thumbnail: $thumbnail, video: $video}';
  }
}