import 'package:flutter/material.dart';
import 'package:sample_flutter_app/view/admin/edit_video_screen.dart';
import 'package:sample_flutter_app/widget/alert_dialog_show.dart';

class ListVideoScreen extends StatefulWidget {
  static const routeName = '/listVideoScreen';
  // const ListVideoScreen({Key key}) : super(key: key);

  @override
  _ListVideoScreenState createState() => _ListVideoScreenState();
}

class _ListVideoScreenState extends State<ListVideoScreen> {
  var alertDialogShow = AlertDialogShow();
  String dropdownValue = 'Desc Post Date';
  List<String> spinnerItems = ['Asc Title', 'Desc Title', 'Asc Post Date', 'Desc Post Date', 'Asc Duration', 'Desc Duration'];
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    Widget _itemVideo() {
      return Padding(
        padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
        child: Container(
          width: width,
          height: 150,
          child: Card(
            child: Row(
              children: <Widget>[
                Expanded(
                    flex: 4,
                    child: Container(
                      color: Colors.grey[100],
                    )),
                Expanded(
                    flex: 6,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                          flex: 7,
                          child: Container(
                            // width: 100,
                            // color: Colors.red,
                            padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text('Title',
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold)),
                                Text('Post Date'),
                                Text('Size'),
                                Text('Duration')
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 3,
                          child: Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                RaisedButton(
                                  color: Colors.green,
                                  onPressed: () {
                                    Navigator.pushNamed(
                                        context, EditVideoScreen.routeName);
                                  },
                                  child: Icon(
                                    Icons.edit,
                                    color: Colors.white,
                                  ),
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                RaisedButton(
                                  color: Colors.red,
                                  onPressed: () {
                                    alertDialogShow.asyncConfirmDialog(
                                        context,
                                        'Are you sure to delete this video ?',
                                        'Yes',
                                        'No');
                                  },
                                  child:
                                      Icon(Icons.delete, color: Colors.white),
                                ),
                              ],
                            ),
                          ),
                        )
                      ],
                    ))
              ],
            ),
          ),
        ),
      );
    }

    Widget _searchVideo() {
      return Container(
        width: width,
        height: height * 0.1,
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: TextField(
            decoration: InputDecoration(
                contentPadding: EdgeInsets.all(5),
                border: OutlineInputBorder(),
                prefixIcon: Icon(Icons.search),
                hintText: 'Search Video'),
          ),
        ),
      );
    }

     Widget _sortVideo() {
      return Container(
        width: width,
        height: height * 0.1,
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: Row(
            children: <Widget>[
              Expanded(flex: 2, child: Text('Sort By: ')),
              Expanded(
                flex: 8,
                child: Container(
                  padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                  decoration:
                      BoxDecoration(border: Border.all(color: Colors.grey)),
                  child: DropdownButton<String>(
                    isExpanded: true,
                    value: dropdownValue,
                    icon: Icon(Icons.arrow_drop_down),
                    iconSize: 24,
                    elevation: 16,
                    // style: TextStyle(color: Colors.red, fontSize: 18),
                    underline: Container(
                      height: 2,
                      color: Colors.transparent,
                    ),
                    onChanged: (String data) {
                      setState(() {
                        dropdownValue = data;
                      });
                    },
                    items: spinnerItems
                        .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    }
    return Column(
        children: <Widget>[
          _searchVideo(),
          _sortVideo(),
          Container(
            width: width,
            height: height * 0.58,
            color: Colors.grey,
            child: ListView(
              children: <Widget>[_itemVideo()],
            ),
          ),
        ],
      );
      // floatingActionButton: FloatingActionButton(
      //   onPressed: () {
      //     Navigator.pushNamed(context, AddVideoScreen.routeName);
      //   },
      //   child: Icon(Icons.add),
      // ),
    
  }
}
