import 'package:flutter/material.dart';
import 'package:sample_flutter_app/view/login_screen.dart';

class DrawerWidgetUser extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          _drawerHeader(),
          _drawerItem(
              icon: Icons.person,
              text: 'Account',
              onTap: () => print('Tap My Files')),
          _drawerItem(
              icon: Icons.money,
              text: 'Buying',
              onTap: () => print('Tap My Files')),
          _drawerItem(
              icon: Icons.logout,
              text: 'Logout',
              onTap: () =>  Navigator.popAndPushNamed(context, LoginScreen.routeName))
        ],
      ),
    );
  }

  Widget _drawerHeader() {
    return UserAccountsDrawerHeader(
      accountName: Text('Hello, Your Name'),
      accountEmail: Text('youremail@mail.com'),
    );
  }

  Widget _drawerItem({IconData icon, String text, GestureTapCallback onTap}) {
    return ListTile(
      title: Row(
        children: <Widget>[
          Icon(icon),
          Padding(
            padding: EdgeInsets.only(left: 25),
            child: Text(
              text,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          )
        ],
      ),
      onTap: onTap,
    );
  }
}
