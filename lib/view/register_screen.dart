import 'package:flutter/material.dart';
import 'package:sample_flutter_app/view/login_screen.dart';
import 'package:sample_flutter_app/view_model/api_services.dart';

class RegisterScreen extends StatefulWidget {
  static const routeName = '/registerScreen';
  const RegisterScreen({Key key}) : super(key: key);

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  var apiServices = APIServices();
  String dropdownValue = 'buyer';


  List<String> spinnerItems = ['buyer', 'seller'];
  var _tfname = TextEditingController();
  var _tfEmail = TextEditingController();
  var _tfPhone = TextEditingController();
  var _tfPassword = TextEditingController();

  

  
  @override
  Widget build(BuildContext context) {
    void _register(){
    apiServices.register(_tfname.text, _tfEmail.text, _tfPhone.text, _tfPassword.text, dropdownValue).then((value){
      print(value);
      if (value.success) {
        Navigator.popAndPushNamed(context, LoginScreen.routeName);
        // Scaffold.of(context).showSnackBar(SnackBar(
        //   content: Text("Registration Succes"),
        // ));
      } else {
        // Scaffold.of(context).showSnackBar(SnackBar(
        //   content: Text("Registration Failed"),
        // ));
        print('failed');
      }
    });
  }
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Padding(
        padding: EdgeInsets.fromLTRB(10, 30, 10, 0),
        child: Container(
          width: width,
          height: height,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text(
                'Register User Account',
                style: TextStyle(fontSize: 20),
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                width: width,
                // height: 100,
                // color: Colors.red,
                child: TextField(
                  controller: _tfname,
                  decoration: InputDecoration(
                      hintText: 'fullname', border: OutlineInputBorder()),
                ),
              ),
               SizedBox(
                height: 15,
              ),
              Container(
                width: width,
                // height: 100,
                // color: Colors.red,
                child: TextField(
                  controller: _tfEmail,
                  decoration: InputDecoration(
                      hintText: 'email', border: OutlineInputBorder()),
                ),
              ),
               SizedBox(
                height: 15,
              ),
              Container(
                width: width,
                // height: 100,
                // color: Colors.red,
                child: TextField(
                  controller: _tfPhone,
                  decoration: InputDecoration(
                      hintText: 'phone', border: OutlineInputBorder()),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                width: width,
                // height: 100,
                // color: Colors.red,
                child: TextField(
                  controller: _tfPassword,
                  obscureText: true,
                  decoration: InputDecoration(
                      hintText: 'password', border: OutlineInputBorder()),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text('Account Type:'),
                  Container(
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey)
                    ),
                     width: width,
                    child: Padding(
                      padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                      child: DropdownButton<String>(
                        isExpanded: true,
                        value: dropdownValue,
                        icon: Icon(Icons.arrow_drop_down),
                        iconSize: 24,
                        elevation: 16,
                        // style: TextStyle(color: Colors.red, fontSize: 18),
                        underline: Container(
                          height: 2,
                          color: Colors.transparent,
                        ),
                        onChanged: (String data) {
                          setState(() {
                            dropdownValue = data;
                          });
                        },
                        items:
                            spinnerItems.map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 15,
              ),
              Stack(
                children: <Widget>[
                  ButtonTheme(
                    minWidth: width,
                    height: height * 0.08,
                    child: RaisedButton(
                      color: Colors.green,
                      onPressed: () {
                        _register();
                      },
                      child: Text(
                        'Register',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 15,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text("Already have account?"),
                  SizedBox(width: 5,),
                  GestureDetector(
                      onTap: () {
                        Navigator.popAndPushNamed(context, LoginScreen.routeName);
                      },
                      child: Text(
                        'Login Account',
                        style: TextStyle(color: Colors.blue),
                      ))
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
