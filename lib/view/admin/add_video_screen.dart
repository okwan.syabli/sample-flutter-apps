import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:sample_flutter_app/view/admin/main_menu_screen.dart';
import 'package:sample_flutter_app/view_model/api_services.dart';

class AddVideoScreen extends StatefulWidget {
  static const routeName = '/addVideoScreen';
  const AddVideoScreen({Key key}) : super(key: key);

  @override
  _AddVideoScreenState createState() => _AddVideoScreenState();
}

class _AddVideoScreenState extends State<AddVideoScreen> {
  PickedFile _videoFile;
  PickedFile _imageFile;
  final ImagePicker _picker = ImagePicker();

  var apiServices = APIServices();

  void _pickImage() async {
    try {
      final pickedFile = await _picker.getImage(source: ImageSource.gallery);
      setState(() {
        _imageFile = pickedFile;
      });
    } catch (e) {
      print("Image picker error " + e.toString());
    }
  }

  void _pickVideo() async {
    try {
      final pickedFile = await _picker.getVideo(source: ImageSource.gallery);
      setState(() {
        _videoFile = pickedFile;
      });
    } catch (e) {
      print("Video picker error " + e.toString());
    }
  }

  void _uploadVideo() {
    apiServices
        .insertVideo('My Video Baba', '7000', '480', '00:40:00', 'baba', 'lorem ipsum', _imageFile, _videoFile)
        .then((value) {
      if (value.success) {
        Navigator.pushReplacementNamed(context, MainMenuScreen.routeName);
      } else {
        print('failed');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      // resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text('Add Video'),
      ),
      body: Padding(
        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
        // child: Container(
        // width: width,
        // height: height,
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text(
                'Add Your Information Video',
                style: TextStyle(fontSize: 20),
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                width: width,
                child: TextField(
                  decoration: InputDecoration(
                      hintText: 'Title', border: OutlineInputBorder()),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                width: width,
                child: TextField(
                  decoration: InputDecoration(
                      hintText: 'Price', border: OutlineInputBorder()),
                ),
              ),
               SizedBox(
                height: 15,
              ),
              Container(
                  width: width,
                  child: RaisedButton(
                    onPressed: () {
                      _pickImage();
                    },
                    child: Text('Choose Thumbnail Video'),
                  )),
              SizedBox(
                height: 15,
              ),
              Container(
                  width: width,
                  child: RaisedButton(
                    onPressed: () {
                      _pickVideo();
                    },
                    child: Text('Choose Video'),
                  )),
              SizedBox(
                height: 15,
              ),
              Row(
                children: <Widget>[
                  Expanded(
                    flex: 4,
                    child: Container(
                      // width: width,
                      child: TextField(
                        decoration: InputDecoration(
                            hintText: 'Width', border: OutlineInputBorder()),
                      ),
                    ),
                  ),
                  Expanded(
                      flex: 2,
                      child: Text(
                        'X',
                        textAlign: TextAlign.center,
                      )),
                  Expanded(
                    flex: 4,
                    child: Container(
                      // width: width,
                      child: TextField(
                        decoration: InputDecoration(
                            hintText: 'Height', border: OutlineInputBorder()),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                width: width,
                child: TextField(
                  maxLines: 5,
                  decoration: InputDecoration(
                      hintText: 'Description', border: OutlineInputBorder()),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Stack(
                children: <Widget>[
                  ButtonTheme(
                    minWidth: width,
                    child: RaisedButton(
                      color: Colors.green,
                      onPressed: () {},
                      child: Text(
                        'Add',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        // ),
      ),
    );
  }
}
