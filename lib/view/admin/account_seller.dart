import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:sample_flutter_app/view/admin/main_menu_screen.dart';
import 'package:sample_flutter_app/view_model/api_services.dart';

class AccountSellerScreen extends StatefulWidget {
  static const routeName = '/accountSellerScreen';
  const AccountSellerScreen({Key key}) : super(key: key);

  @override
  _AccountSellerScreenState createState() => _AccountSellerScreenState();
}

class _AccountSellerScreenState extends State<AccountSellerScreen> {
  final ImagePicker _picker = ImagePicker();
  PickedFile _imageFile;

  var apiServices = APIServices();

  void _pickPhoto() async {
    try {
      final pickedFile = await _picker.getImage(source: ImageSource.gallery);
      setState(() {
        _imageFile = pickedFile;
      });
    } catch (e) {
      print("Image picker error " + e.toString());
    }
  }

  void _uploadImage() {
    apiServices
        .insertPhoto('Baba', '50000', '360', 'baba', 'lorem', _imageFile)
        .then((value) {
      if (value.success) {
        Navigator.pushReplacementNamed(context, MainMenuScreen.routeName);
      } else {
        print('failed');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text('Account'),
      ),
      body: Padding(
        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
        child: Container(
          width: width,
          height: height,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text(
                'My Account (Seller)',
                style: TextStyle(fontSize: 20),
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                width: width,
                child: TextField(
                  decoration: InputDecoration(
                      hintText: 'Name', border: OutlineInputBorder()),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                width: width,
                child: TextField(
                  decoration: InputDecoration(
                      hintText: 'Email', border: OutlineInputBorder()),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                width: width,
                child: TextField(
                  decoration: InputDecoration(
                      hintText: 'Phone Number', border: OutlineInputBorder()),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                width: width,
                child: TextField(
                  maxLines: 4,
                  decoration: InputDecoration(
                      hintText: 'Address', border: OutlineInputBorder()),
                ),
              ),
              Stack(
                children: <Widget>[
                  ButtonTheme(
                    minWidth: width,
                    child: RaisedButton(
                      color: Colors.green,
                      onPressed: () {
                        _uploadImage();
                      },
                      child: Text(
                        'Update',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
