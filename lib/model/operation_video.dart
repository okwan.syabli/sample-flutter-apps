class OperationVideo {
  final bool success;
  final String message;

  OperationVideo({
    this.success,
    this.message
  });

  factory OperationVideo.fromJson(Map<String, dynamic> json){
    return OperationVideo(
      success: json['success'],
      message: json['message'],
    );
  }

  @override
  String toString() {
    return 'OperationVideo{success: $success, message: $message}';
  }
}