import 'package:flutter/material.dart';
import 'package:sample_flutter_app/view/admin/edit_video_screen.dart';
import 'package:sample_flutter_app/widget/alert_dialog_show.dart';

class SearchResultVideoScreen extends StatefulWidget {
  static const routeName = '/searchResultVideoScreen';
  // const SearchResultVideoScreen({Key key}) : super(key: key);

  @override
  _SearchResultVideoScreenState createState() =>
      _SearchResultVideoScreenState();
}

class _SearchResultVideoScreenState extends State<SearchResultVideoScreen> {
  var alertDialogShow = AlertDialogShow();
  String dropdownValue = 'Desc Post Date';
  List<String> spinnerItems = [
    'Asc Title',
    'Desc Title',
    'Asc Post Date',
    'Desc Post Date',
    'Asc Duration',
    'Desc Duration'
  ];
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    Widget _itemVideo() {
      return Padding(
        padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
        child: Container(
          width: width,
          height: 150,
          child: Card(
            child: Row(
              children: <Widget>[
                Expanded(
                    flex: 4,
                    child: Container(
                      color: Colors.grey[100],
                    )),
                Expanded(
                    flex: 6,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                          flex: 7,
                          child: Container(
                            // width: 100,
                            // color: Colors.red,
                            padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text('Title',
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold)),
                                Text('Post Date'),
                                Text('Size'),
                                Text('Duration')
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 3,
                          child: Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                RaisedButton(
                                  color: Colors.green,
                                  onPressed: () {
                                    Navigator.pushNamed(
                                        context, EditVideoScreen.routeName);
                                  },
                                  child: Icon(
                                    Icons.edit,
                                    color: Colors.white,
                                  ),
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                RaisedButton(
                                  color: Colors.red,
                                  onPressed: () {
                                    alertDialogShow.asyncConfirmDialog(
                                        context,
                                        'Are you sure to delete this video ?',
                                        'Yes',
                                        'No');
                                  },
                                  child:
                                      Icon(Icons.delete, color: Colors.white),
                                ),
                              ],
                            ),
                          ),
                        )
                      ],
                    ))
              ],
            ),
          ),
        ),
      );
    }

    return Scaffold(
      appBar: AppBar(title: Text('Result Video'),),
        body: Container(
      width: width,
      height: height,
      color: Colors.grey,
      child: ListView(
        children: <Widget>[_itemVideo()],
      ),
    ));
  }
}
