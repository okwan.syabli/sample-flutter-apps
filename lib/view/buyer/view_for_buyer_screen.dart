import 'package:flutter/material.dart';
import 'package:sample_flutter_app/view/buyer/list_photo_for_buyer_screen.dart';
import 'package:sample_flutter_app/view/buyer/list_video_for_buyer_screen.dart';
import 'package:sample_flutter_app/widget/drawer_widget_buyer.dart';

class ViewForBuyerScreen extends StatefulWidget {
  static const routeName = '/viewForBuyerScreen';
  @override
  _ViewForBuyerScreenState createState() => _ViewForBuyerScreenState();
}

class _ViewForBuyerScreenState extends State {
  List<Widget> itemTabBar = [
    Tab(text: 'List Photo'),
    Tab(text: 'List Video'),
  ];

  List<Widget> itemTabBarView = [ListPhotoForBuyerScreen(), ListVideoForBuyerScreen()];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Catalogue'),
      ),
      drawer: DrawerWidgetUser(),
      body: DefaultTabController(
        length: 2,
        child: Column(
          children: [
            Container(
              child: TabBar(labelColor: Colors.black, tabs: itemTabBar),
            ),
            Expanded(
              child: TabBarView(children: itemTabBarView),
            ),
          ],
        ),
      ),
    );
  }
}
