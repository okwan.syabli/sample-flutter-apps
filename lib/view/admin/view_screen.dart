import 'package:flutter/material.dart';
import 'package:sample_flutter_app/view/admin/list_view_photo_screen.dart';
import 'package:sample_flutter_app/view/admin/list_view_video_screen.dart';

class ViewScreen extends StatefulWidget {
  static const routeName = '/viewScreen';
  @override
  _ViewScreenState createState() => _ViewScreenState();
}

class _ViewScreenState extends State {
  List<Widget> itemTabBar = [
    Tab(text: 'List Photo'),
    Tab(text: 'List Video'),
  ];

  List<Widget> itemTabBarView = [ListViewPhotoScreen(), ListViewVideoScreen()];
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Column(
        children: [
          Container(
            child: TabBar(labelColor: Colors.black, tabs: itemTabBar),
          ),
          Expanded(
            child: TabBarView(children: itemTabBarView),
          ),
        ],
      ),
    );
  }
}
