import 'package:flutter/material.dart';

class CheckoutScreen extends StatefulWidget {
  static const routeName = '/checkoutScreen';
  const CheckoutScreen({Key key}) : super(key: key);

  @override
  _CheckoutScreenState createState() => _CheckoutScreenState();
}

class _CheckoutScreenState extends State<CheckoutScreen> {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        title: Text('Checkout'),
      ),
      body: Container(
        alignment: Alignment.center,
        width: width,
        height: height,
        color: Colors.grey,
        child: Container(
          width: width * 0.8,
          height: height * 0.5,
          child: Card(
            child: Column(
              children: <Widget>[
                 Expanded(
                    child: Container(
                      alignment: Alignment.center,
                        child: Text(
                  'Video',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                ))),
                Expanded(
                    child: Container(
                      alignment: Alignment.center,
                        child: Text(
                  'Title',
                  style: TextStyle( fontSize: 18,),
                ))),
                Expanded(
                  flex: 4,
                  child: Container(
                     alignment: Alignment.center,
                     color: Colors.grey[200],
                    child: Text('Image'))),
                Expanded(child: Container(
                   alignment: Alignment.center,
                  child: Text('Price'))),
                Expanded(
                    child: ButtonTheme(
                  minWidth: width,
                  child: RaisedButton(
                    onPressed: () {},
                    child: Text('CHECKOUT', style: TextStyle(color: Colors.white),),
                  ),
                ))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
