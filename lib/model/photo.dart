class Photo {
  final bool success;
  final String message;
  final List<dynamic> data;

  Photo({
    this.success,
    this.message,
    this.data
  });

  factory Photo.fromJson(Map<String, dynamic> json){
    return Photo(
      success: json['success'],
      message: json['message'],
      data: json['data']
    );
  }

  @override
  String toString() {
    return 'Photo{success: $success, message: $message, data: $data}';
  }
}

class PhotoData {
  int id;
  String title;
  String price;
  String resolution;
  String uploader;
  String description;
  String photo;
  PhotoData({this.id, this.title, this.price, this.resolution, this.uploader, this.description, this.photo});

  factory PhotoData.fromJson(Map<String, dynamic> json) {
    return PhotoData(
        id: json['id'],
        title: json['title'],
        price: json['price'],
        resolution: json['resolution'],
        uploader: json['uploader'],
        description: json['description'],
        photo: json['photo']
        );
  }

  @override
  String toString() {
    return 'PhotoData{id: $id, title: $title, price: $price, resolution: $resolution, uploader: $uploader, description: $description, photo: $photo}';
  }
}