import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:sample_flutter_app/model/arguments.dart';
import 'package:sample_flutter_app/model/detail_photo.dart';
import 'package:sample_flutter_app/view/admin/main_menu_screen.dart';
import 'package:sample_flutter_app/view_model/api_services.dart';
import 'package:shared_preferences/shared_preferences.dart';

class EditPhotoScreen extends StatefulWidget {
  static const routeName = '/editPhotoScreen';
  const EditPhotoScreen({Key key}) : super(key: key);

  @override
  _EditPhotoScreenState createState() => _EditPhotoScreenState();
}

class _EditPhotoScreenState extends State<EditPhotoScreen> {
  final ImagePicker _picker = ImagePicker();
  PickedFile _imageFile;

  String dropdownValue = '64 px';
  List<String> spinnerItems = ['64 px', '128 px', '360 px', '480 pc'];

  var apiServices = APIServices();
  TextEditingController tfTitle = TextEditingController();
  TextEditingController tfPrice = TextEditingController();
  TextEditingController tfUploader = TextEditingController();
  TextEditingController tfDescription = TextEditingController();

  @override
  void initState() {
    super.initState();
  
  }

  void _pickPhoto() async {
    try {
      final pickedFile = await _picker.getImage(source: ImageSource.gallery);
      setState(() {
        _imageFile = pickedFile;
      });
    } catch (e) {
      print("Image picker error " + e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    final DetailPhoto args = ModalRoute.of(context).settings.arguments;
    tfTitle.text = args.title;
    tfPrice.text = args.price;
    tfDescription.text = args.description;
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    void _updateImage() async{
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String _email = prefs.getString('email');
      apiServices
          .updatePhoto(
              tfTitle.text.toString(),
              tfPrice.text.toString(),
              dropdownValue.toString(),
              _email.toString(),
              tfDescription.text.toString(),
              _imageFile,
              args.id)
          .then((value) {
            print(value);
        if (value.success) {
          Navigator.pushReplacementNamed(context, MainMenuScreen.routeName);
        } else {
          print('failed');
          // Scaffold.of(context).showSnackBar(SnackBar(
          //   content: Text("Insert Photo Failed"),
          // ));
        }
      });
    }

    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text('Edit Photo'),
        
      ),

      body: Padding(
        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
        child: Container(
          width: width,
          height: height,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text(
                'Edit Your Information Photo',
                style: TextStyle(fontSize: 20),
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                width: width,
                child: TextField(
                  controller: tfTitle,
                  decoration: InputDecoration(
                      hintText: args.title, border: OutlineInputBorder()
                      ),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                width: width,
                child: TextField(
                  controller: tfPrice,
                  decoration: InputDecoration(
                      hintText: args.price, border: OutlineInputBorder()),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                decoration:
                    BoxDecoration(border: Border.all(color: Colors.grey)),
                width: width,
                height: 60,
                child: DropdownButton<String>(
                  isExpanded: true,
                  value: dropdownValue,
                  icon: Icon(Icons.arrow_drop_down),
                  iconSize: 24,
                  elevation: 16,
                  // style: TextStyle(color: Colors.red, fontSize: 18),
                  underline: Container(
                    height: 2,
                    color: Colors.transparent,
                  ),
                  onChanged: (String data) {
                    setState(() {
                      dropdownValue = data;
                    });
                  },
                  items: spinnerItems
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                  width: width,
                  child: RaisedButton(
                    onPressed: () {
                      _pickPhoto();
                    },
                    child: Text('Choose Photo'),
                  )),
              SizedBox(
                height: 15,
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                width: width,
                child: TextField(
                  controller: tfDescription,
                  maxLines: 5,
                  decoration: InputDecoration(
                      hintText: args.description, border: OutlineInputBorder()),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Stack(
                children: <Widget>[
                  ButtonTheme(
                    minWidth: width,
                    height: 60,
                    child: RaisedButton(
                      color: Colors.green,
                      onPressed: () {
                        _updateImage();
                      },
                      child: Text(
                        'Update',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
