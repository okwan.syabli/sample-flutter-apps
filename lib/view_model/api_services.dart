import 'dart:convert';
import 'package:image_picker/image_picker.dart';
import 'package:sample_flutter_app/model/login.dart';
import 'package:http/http.dart' as http;
import 'package:sample_flutter_app/model/operation_photo.dart';
import 'package:sample_flutter_app/model/operation_video.dart';
import 'package:sample_flutter_app/model/photo.dart';
import 'package:sample_flutter_app/model/register.dart';
import 'package:sample_flutter_app/model/video.dart';
import 'package:shared_preferences/shared_preferences.dart';

class APIServices {
  // final baseUrl = 'http://192.168.43.160/api';
  // final baseUrlAlt = 'http://192.168.43.160';

  final baseUrl = 'http://192.168.8.101/api';
  final baseUrlAlt = 'http://192.168.8.101';

  Future<Login> login(String email, String password) async {
    final response = await http
        .post('$baseUrl/login', body: {'email': email, 'password': password});
    try {
      if (response.statusCode == 200) {
        print(Login.fromJson(jsonDecode(response.body)));
        var result = Login.fromJson(jsonDecode(response.body));
        return result;
      } else {
        return Login(success: false, message: 'Login Failed', token: '');
      }
    } catch (e) {
      return Login(success: false, message: 'Login Failed', token: '');
    }
  }

  Future<Register> register(String name, String email, String phone,
      String password, String type) async {
    final response = await http.post('$baseUrl/register', body: {
      'name': email,
      'email': email,
      'phone': phone,
      'password': password,
      'password_confirmation': password,
      'type': type
    });
    try {
      if (response.statusCode == 200) {
        print(Register.fromJson(jsonDecode(response.body)));
        var result = Register.fromJson(jsonDecode(response.body));
        return result;
      } else {
        print(jsonDecode(response.body));
        return Register(success: false, message: 'Register Failed', token: '');
      }
    } catch (e) {
      print(jsonDecode(response.body));
      return Register(success: false, message: 'Register Failed', token: '');
    }
  }

  // =================================CRUD PHOTO

  Future<Photo> listPhoto(String token) async {
    final response = await http.get('$baseUrl/seller/photo',
        headers: {'Authorization': 'bearer $token'});
    try {
      if (response.statusCode == 200) {
        print(Photo.fromJson(jsonDecode(response.body)));
        var result = Photo.fromJson(jsonDecode(response.body));
        return result;
      } else {
        return Photo(success: false, message: 'Login Failed', data: []);
      }
    } catch (e) {
      return Photo(success: false, message: 'Login Failed', data: []);
    }
  }

  Future<OperationPhoto> insertPhoto(
      String title,
      String price,
      String resolution,
      String uploader,
      String description,
      PickedFile imageFile) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var _token = prefs.getString('token');
    print(title);
    print(price);
    print(resolution);
    print(uploader);
    print(description);
    print(imageFile.path);

    var request =
        http.MultipartRequest('POST', Uri.parse(baseUrl + '/seller/photo'));
    request.headers['Authorization'] = 'bearer $_token';
    request.headers['Content-Type'] = 'multipart/form-data';
    request.fields['title'] = title;
    request.fields['price'] = price;
    request.fields['resolution'] = resolution;
    request.fields['uploader'] = uploader;
    request.fields['description'] = description;
    request.files
        .add(await http.MultipartFile.fromPath('photo', imageFile.path));
    var res = await request.send();
    try {
      var responseData = await res.stream.toBytes();
      var responseString = String.fromCharCodes(responseData);
      print(responseString);
      return OperationPhoto.fromJson(jsonDecode(responseString));
    } catch (e) {
      return OperationPhoto(success: false, message: 'Insert Photo Failed');
    }
  }

  Future<OperationPhoto> updatePhoto(
      String title,
      String price,
      String resolution,
      String uploader,
      String description,
      PickedFile imageFile,
      String id) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var _token = prefs.getString('token');
    var request = http.MultipartRequest(
        'POST', Uri.parse(baseUrl + '/seller/photo/' + id));
    

    if (imageFile?.path == null) {
      request.headers['Authorization'] = 'bearer $_token';
      request.headers['Content-Type'] = 'multipart/form-data';
      request.fields['title'] = title;
      request.fields['price'] = price;
      request.fields['resolution'] = resolution;
      request.fields['uploader'] = uploader;
      request.fields['description'] = description;
    } else {
      // print(imageFile.path);
    // print(id);
      request.headers['Authorization'] = 'bearer $_token';
      request.headers['Content-Type'] = 'multipart/form-data';
      request.fields['title'] = title;
      request.fields['price'] = price;
      request.fields['resolution'] = resolution;
      request.fields['uploader'] = uploader;
      request.fields['description'] = description;
      request.files
          .add(await http.MultipartFile.fromPath('photo', imageFile.path));
    }
     var res = await request.send();
    try {
      var responseData = await res.stream.toBytes();
      var responseString = String.fromCharCodes(responseData);
      print(jsonDecode(responseString));
      return OperationPhoto.fromJson(jsonDecode(responseString));
    } catch (e) {
      print(jsonDecode(e));
      return OperationPhoto(success: false, message: 'Update Photo Failed');
    }
  }

  Future<OperationPhoto> deletePhoto(String id) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var _token = prefs.getString('token');
    final response = await http.get('$baseUrl/seller/photo/delete/' + id,
        headers: {'Authorization': 'bearer $_token'});
    try {
      if (response.statusCode == 200) {
        print(OperationPhoto.fromJson(jsonDecode(response.body)));
        var result = OperationPhoto.fromJson(jsonDecode(response.body));
        return result;
      } else {
        return OperationPhoto(success: false, message: 'Delete Failed');
      }
    } catch (e) {
      return OperationPhoto(success: false, message: 'Delete Failed');
    }
  }

  // =================================CRUD VIDEO

  Future<Video> listVideo() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var _token = prefs.getString('token');
    final response = await http.get('$baseUrl/seller/video',
        headers: {'Authorization': 'bearer $_token'});
    try {
      if (response.statusCode == 200) {
        print(Video.fromJson(jsonDecode(response.body)));
        var result = Video.fromJson(jsonDecode(response.body));
        return result;
      } else {
        return Video(success: false, message: 'List Photo', data: []);
      }
    } catch (e) {
      return Video(success: false, message: 'List Photo', data: []);
    }
  }

  Future<OperationVideo> insertVideo(
      String title,
      String price,
      String resolution,
      String duration,
      String uploader,
      String description,
      PickedFile imageFile,
      PickedFile videoFile) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var _token = prefs.getString('token');
    var request =
        http.MultipartRequest('POST', Uri.parse(baseUrl + '/seller/photo'));
    request.headers['Authorization'] = 'bearer $_token';
    request.headers['Content-Type'] = 'multipart/form-data';
    request.fields['title'] = title;
    request.fields['price'] = price;
    request.fields['resolution'] = resolution;
    request.fields['duration'] = duration;
    request.fields['uploader'] = uploader;
    request.fields['description'] = description;
    request.files
        .add(await http.MultipartFile.fromPath('thumnail', imageFile.path));
    request.files
        .add(await http.MultipartFile.fromPath('video', videoFile.path));
    var res = await request.send();
    try {
      var responseData = await res.stream.toBytes();
      var responseString = String.fromCharCodes(responseData);
      return OperationVideo.fromJson(jsonDecode(responseString));
    } catch (e) {
      return OperationVideo(success: false, message: 'Insert Video Failed');
    }
  }

  Future<OperationVideo> updateVideo(
      String title,
      String price,
      String resolution,
      String duration,
      String uploader,
      String description,
      PickedFile imageFile,
      PickedFile videoFile,
      String id) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var _token = prefs.getString('token');
    var request = http.MultipartRequest(
        'POST', Uri.parse(baseUrl + '/seller/photo/' + id));
    request.headers['Authorization'] = 'bearer $_token';
    request.headers['Content-Type'] = 'multipart/form-data';
    request.fields['title'] = title;
    request.fields['price'] = price;
    request.fields['resolution'] = resolution;
    request.fields['duration'] = duration;
    request.fields['uploader'] = uploader;
    request.fields['description'] = description;
    request.files
        .add(await http.MultipartFile.fromPath('thumnail', imageFile.path));
    request.files
        .add(await http.MultipartFile.fromPath('video', videoFile.path));
    var res = await request.send();
    try {
      var responseData = await res.stream.toBytes();
      var responseString = String.fromCharCodes(responseData);
      return OperationVideo.fromJson(jsonDecode(responseString));
    } catch (e) {
      return OperationVideo(success: false, message: 'Update Video Failed');
    }
  }

  Future<OperationVideo> deleteVideo(String id) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var _token = prefs.getString('token');
    final response = await http.get('$baseUrl/seller/video/delete/' + id,
        headers: {'Authorization': 'bearer $_token'});
    try {
      if (response.statusCode == 200) {
        print(OperationVideo.fromJson(jsonDecode(response.body)));
        var result = OperationVideo.fromJson(jsonDecode(response.body));
        return result;
      } else {
        return OperationVideo(success: false, message: 'Delete Video Failed');
      }
    } catch (e) {
      return OperationVideo(success: false, message: 'Delete Video Failed');
    }
  }
}
