import 'package:flutter/material.dart';
import 'package:sample_flutter_app/view/admin/account_seller.dart';
import 'package:sample_flutter_app/view/admin/add_photo_screen.dart';
import 'package:sample_flutter_app/view/admin/add_video_screen.dart';
import 'package:sample_flutter_app/view/admin/edit_photo_screen.dart';
import 'package:sample_flutter_app/view/admin/edit_video_screen.dart';
import 'package:sample_flutter_app/view/admin/list_photo_screen.dart';
import 'package:sample_flutter_app/view/admin/list_video_screen.dart';
import 'package:sample_flutter_app/view/login_screen.dart';
import 'package:sample_flutter_app/view/admin/main_menu_screen.dart';
import 'package:sample_flutter_app/view/buyer/checkout_screen.dart';
import 'package:sample_flutter_app/view/buyer/view_for_buyer_screen.dart';
import 'package:sample_flutter_app/view/register_screen.dart';
import 'package:sample_flutter_app/view/search_result_photo_screen.dart';
import 'package:sample_flutter_app/view/search_result_video_screen.dart';
import 'package:sample_flutter_app/view/splash_screen.dart';
import 'package:sample_flutter_app/view/admin/view_screen.dart';
import 'package:sample_flutter_app/widget/my_image_picker.dart';

class Routes extends StatelessWidget {
  const Routes({ Key key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: '/loginScreen',
      routes: {
        SplashScreen.routeName: (context) => SplashScreen(),
        LoginScreen.routeName: (context) => LoginScreen(),
        RegisterScreen.routeName: (context) => RegisterScreen(),
        ListPhotoScreen.routeName: (context) => ListPhotoScreen(),
        AddPhotoScreen.routeName: (context) => AddPhotoScreen(),
        EditPhotoScreen.routeName: (context) => EditPhotoScreen(),
        ListVideoScreen.routeName: (context) => ListVideoScreen(),
        AddVideoScreen.routeName: (context) => AddVideoScreen(),
        EditVideoScreen.routeName: (context) => EditVideoScreen(),
        MainMenuScreen.routeName: (context) => MainMenuScreen(),
        ViewScreen.routeName: (context) => ViewScreen(),
        ViewForBuyerScreen.routeName: (context) => ViewForBuyerScreen(),
        CheckoutScreen.routeName: (context) => CheckoutScreen(),
        SearchResultPhotoScreen.routeName: (context) => SearchResultPhotoScreen(),
        SearchResultVideoScreen.routeName: (context) => SearchResultVideoScreen(),
        MyImagePicker.routeName: (context) => MyImagePicker(),
        AccountSellerScreen.routeName: (context) => AccountSellerScreen()
      },
    );
  }
}