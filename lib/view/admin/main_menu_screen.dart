import 'package:flutter/material.dart';
import 'package:sample_flutter_app/view/admin/add_photo_screen.dart';
import 'package:sample_flutter_app/view/admin/add_video_screen.dart';
import 'package:sample_flutter_app/view/admin/list_photo_screen.dart';
import 'package:sample_flutter_app/view/admin/list_video_screen.dart';
import 'package:sample_flutter_app/view/admin/view_screen.dart';
import 'package:sample_flutter_app/widget/drawer_widget.dart';

class MainMenuScreen extends StatefulWidget {
  static const routeName = '/mainMenuScreen';
  @override
  _MainMenuScreenState createState() => _MainMenuScreenState();
}

class _MainMenuScreenState extends State<MainMenuScreen> {
  int _selectedTabIndex = 0;

  void _onNavBarTapped(int index) {
    setState(() {
      _selectedTabIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    final _listPage = <Widget>[
      ListPhotoScreen(),
      ListVideoScreen(),
      ViewScreen(),
    ];

    final _bottomNavBarItems = <BottomNavigationBarItem>[
      BottomNavigationBarItem(icon: Icon(Icons.photo), title: Text("Photo")),
      BottomNavigationBarItem(
          icon: Icon(Icons.video_label), title: Text("Video")),
      BottomNavigationBarItem(
          icon: Icon(Icons.remove_red_eye_sharp), title: Text("View")),
    ];

    final _bottomNavBar = BottomNavigationBar(
      items: _bottomNavBarItems,
      currentIndex: _selectedTabIndex,
      selectedItemColor: Colors.blueAccent,
      unselectedItemColor: Colors.grey,
      onTap: _onNavBarTapped,
    );
    return Scaffold(
      appBar: AppBar(
        title: _selectedTabIndex == 0
            ? Text('Photo')
            : _selectedTabIndex == 1
                ? Text('Video')
                : Text('View'),
      ),
      drawer: DrawerWidget(),
      body: Center(
        child: _listPage[_selectedTabIndex],
      ),
      bottomNavigationBar: _bottomNavBar,
      floatingActionButton: Visibility(
        visible: _selectedTabIndex == 2 ?false : true,
        child: FloatingActionButton(
          onPressed: () {
            _selectedTabIndex == 0 ? Navigator.pushNamed(context, AddPhotoScreen.routeName): Navigator.pushNamed(context, AddVideoScreen.routeName);
          },
          child: Icon(Icons.add),
        ),
      ),
    );
  }
}
